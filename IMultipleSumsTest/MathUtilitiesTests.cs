using IMultipleSums;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        [Test]
        public void SumOf0To10Divisors3and5()
        {
            //Arrange
            const int expected = 23;

            //Act
            var actual = MathUtilities.AddRangeIfMultipleOf(1, 9, new[] { 3, 5 });

            //Arrange
            Assert.That(actual.Equals(expected));
        }

        [Test]
        public void IsAMultipleOf3Test()
        {
            // Arrange
            const int target = 9;
            const bool expected = true;

            // Act
            var actual = MathUtilities.IsMultipleOf(target, 3);

            // Assert
            Assert.That(actual.Equals(expected));
        }

        [Test]
        public void IsNotAMultipleOf3Test()
        {
            // Arrange
            const int target = 8;
            const bool expected = false;

            // Act
            var actual = MathUtilities.IsMultipleOf(target, 3);

            // Assert
            Assert.That(actual.Equals(expected));
        }

        [Test]
        public void IsAMultipleOf5Test()
        {
            // Arrange
            const int target = 125;
            const bool expected = true;

            // Act
            var actual = MathUtilities.IsMultipleOf(target, 5);

            // Assert
            Assert.That(actual.Equals(expected));
        }

        [Test]
        public void IsNotAMultipleOf5Test()
        {
            // Arrange
            const int target = 126;
            const bool expected = false;

            // Act
            var actual = MathUtilities.IsMultipleOf(target, 5);
            
            // Assert
            Assert.That(actual.Equals(expected));
        }
    }
}