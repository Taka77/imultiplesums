﻿using IMultipleSums;
using NUnit.Framework;

namespace IMultipleSumsTest
{
    public class ProgramTests
    {
        [Test]
        public void PrintJust1DivisorTest()
        {
            //Arrange
            const string expected = "4";
            var divisors = new[] {4};

            //Act
            var actual = StringUtilities.PrintDivisors(divisors);

            //Arrange
            Assert.That(actual.Equals(expected));
        }

        [Test]
        public void PrintJust2DivisorsTest()
        {
            //Arrange
            const string expected = "4 and 5";
            var divisors = new[] {4, 5};

            //Act
            var actual = StringUtilities.PrintDivisors(divisors);

            //Arrange
            Assert.That(actual.Equals(expected));
        }

        [Test]
        public void Print3DivisorsTest()
        {
            //Arrange
            const string expected = "4, 5 and 6";
            var divisors = new[] {4, 5, 6};

            //Act
            var actual = StringUtilities.PrintDivisors(divisors);

            //Arrange
            Assert.That(actual.Equals(expected));
        }
    }
}