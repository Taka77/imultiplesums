﻿using System.Collections.Generic;
using System.Text;

namespace IMultipleSums
{
    public static class StringUtilities
    {
        public static string PrintDivisors(IReadOnlyList<int> divisors)
        {
            var divisorString = new StringBuilder();
            for (var i = 0; i < divisors.Count; i++)
            {
                divisorString.Append(divisors[i]);
                if (i < divisors.Count - 2)
                {
                    divisorString.Append(", ");
                }
                else if (i < divisors.Count - 1)
                {
                    divisorString.Append(" and ");
                }
            }

            return divisorString.ToString();
        }
    }
}