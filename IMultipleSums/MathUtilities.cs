﻿using System.Linq;

namespace IMultipleSums
{
    public static class MathUtilities
    {
        public static bool IsMultipleOf(this int target, int divisor)
        {
            return target % divisor == 0;
        }

        public static int AddRangeIfMultipleOf(int start, int end, int[] divisors)
        {
            var sum = 0;
            for (var i = start; i <= end; i++)
            {
                if (divisors.Any(divisor => i.IsMultipleOf(divisor)))
                    sum += i;
            }

            return sum;
        }
    }
}