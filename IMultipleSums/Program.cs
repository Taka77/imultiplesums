﻿using System;

namespace IMultipleSums
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var divisors = new[] {3, 5};
            const int start = 1;
            const int end = 999;
            var sum = MathUtilities.AddRangeIfMultipleOf(start, end, divisors);

            Console.WriteLine(
                $"The sum of all multiples of {StringUtilities.PrintDivisors(divisors)} from {start} to {end} is {sum}");
        }
    }
}